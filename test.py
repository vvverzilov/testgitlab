import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pandahouse as ph
from datetime import datetime, timedelta, date
import io
import telegram

connection = {'host': 'http://clickhouse.beslan.pro:8080',
              'database':'simulator_20220620',
              'user':'student',
              'password':'dpo_python_2020'
             }
 
metrics_mess = ['users_mess', 'count_mess']

metrics_feed = ['users_feed', 'views', 'likes', 'ctr']

def check_anomaly(df, metric, a=4, n=5):
    df['q25'] = df[metric].shift(1).rolling(n).quantile(0.25)
    df['q75'] = df[metric].shift(1).rolling(n).quantile(0.75)
    df['iqr'] = df['q75'] - df['q25'] # межквартильный размах
    df['up'] = df['q75'] + a*df['iqr'] # верхняя граница
    df['low'] = df['q25'] - a*df['iqr'] # нижняя граница

    #сглаживание верхних и нижних границ чтобы небыло слишком зубчатых границ и неприемлимых срабатываний
    df['up'] = df['up'].rolling(n, center=True, min_periods = 1).mean()
    df['low'] = df['low'].rolling(n, center=True, min_periods = 1).mean()

    # iloc(-1) - наша самая последняя пятнадцатиминутка    
    if df[metric].iloc[-1] < df['low'].iloc[-1] or df[metric].iloc[-1] > df['up'].iloc[-1]:
        is_alert = 1
    else:
        is_alert = 0

    return is_alert, df 

def send_anomaly(df, metric, msg):

    # Получаем доступ к боту
    my_token = '5566764152:AAEQfbMb-uw6rG97fk6D2KLm9DOjqw27Ld0'
    bot = telegram.Bot(token=my_token) # получаем доступ
    chat_id = -1001567206873 # -714461138 мой -1001567206873 алерты -519115532

    sns.set(rc={'figure.figsize': (16, 10)}) # задаем размер графика
    plt.tight_layout()

    # рисуем 3 линии
    ax = sns.lineplot(x=df['ts'], y=df[metric], label = 'current')
    ax = sns.lineplot(x=df['ts'], y=df['up'], label = 'up')
    ax = sns.lineplot(x=df['ts'], y=df['low'], label = 'low')

    # этот цикл нужен чтобы разрядить подписи координат по оси Х, что бы подпись была не для каждого значения
    for ind, label in enumerate(ax.get_xticklabels()):  
        if ind % 2 == 0:
            label.set_visible(True)
        else:
            label.set_visible(False)

    ax.set(xlabel='time') # задаем имя оси Х
    ax.set(ylabel=metric) # задаем имя оси У

    ax.set_title('{}'.format(metric)) # задае заголовок графика
    ax.set(ylim=(0, None)) # задаем лимит для оси У

    # формируем файловый объект
    plot_object = io.BytesIO()
    ax.figure.savefig(plot_object)
    plot_object.seek(0)
    plot_object.name = '{0}.png'.format(metric)
    plt.close()

    # отправляем алерт
    bot.sendMessage(chat_id=chat_id, text=msg)
    bot.sendPhoto(chat_id=chat_id, photo=plot_object) 
    print(msg)
    return

  
def load_feed(): 
        # количество уникальных пользователей со вчера по настоящее время с разбивкой 15 мин
        # для удобства построения графиков в запрос добавиль колонки даты и время
        # ts < toStartOfFifteenMinutes(now()) - берем текущее время, округляем до начала пятнадцатиминутки 
        # и ее не берем т.к. она может быть не полная и можем получить некорректный алерт        
    q_feed = ''' 
    SELECT
        toStartOfFifteenMinutes(time) as ts, 
        toDate(ts) as date, 
        formatDateTime(ts, '%R') as hm, 
        uniqExact(user_id) as users_feed,
        countIf(user_id, action = 'view') as views,
        countIf(user_id, action = 'like') as likes,
        likes / views as ctr
    FROM simulator_20220620.feed_actions
    WHERE time >=  today() - 1 and time < toStartOfFifteenMinutes(now())
    GROUP BY ts, date, hm
    ORDER BY ts 
    '''
    
    df_v = ph.read_clickhouse(query=q_feed, connection = connection)

    search_anomaly(df_v, metrics_feed)

    return df_v
    

def load_mess(): 
    
    q_mess = ''' 
    SELECT
        toStartOfFifteenMinutes(time) as ts, 
        toDate(ts) as date, 
        formatDateTime(ts, '%R') as hm, 
        uniqExact(user_id) as users_mess,
        count(user_id) as count_mess
    FROM simulator_20220620.message_actions
    WHERE time >=  today() - 1 and time < toStartOfFifteenMinutes(now())
    GROUP BY ts, date, hm
    ORDER BY ts
    '''

    df_v = ph.read_clickhouse(query=q_mess, connection = connection)

    search_anomaly(df_v, metrics_mess) 

    return df_v    


def search_anomaly(df_v, metrics_v): 
    
    for metric in metrics_v: 
        df = df_v[['ts','date','hm', metric]].copy()# будет создан новый объект с копией данных и индексов в
            #ызывающего объекта. Изменения данных или индексов копии не будут отражены в исходном объекте

        is_alert, df = check_anomaly(df, metric)        

        if is_alert == 1 or True: # если добавить условие or True - оно всегда будет срабатывать, использовать для теста
            # .2% округляет до 2 знаков после запятой и переводит в проценты
            msg = '''Метрика {metric}:\nтекущее значение = {current_value:.2f}\nотклонение от вчера {last_value_diff:.2%}'''\
                .format(metric=metric,
                        current_value=df[metric].iloc[-1], 
                        last_value_diff=abs(1-df[metric].iloc[-1]/df[metric].iloc[-2]))
            send_anomaly(df, metric, msg)                                
        else:
            print(metric, ': no anomaly')

    return

load_feed()
load_mess()

print("Test branch")